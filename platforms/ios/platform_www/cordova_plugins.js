cordova.define('cordova/plugin_list', function(require, exports, module) {
  module.exports = [
    {
      "id": "cordova-universal-links-plugin.universalLinks",
      "file": "plugins/cordova-universal-links-plugin/www/universal_links.js",
      "pluginId": "cordova-universal-links-plugin",
      "clobbers": [
        "universalLinks"
      ]
    },
    {
      "id": "kunder-cordova-plugin-webview.webview",
      "file": "plugins/kunder-cordova-plugin-webview/www/webViewPlugin.js",
      "pluginId": "kunder-cordova-plugin-webview",
      "merges": [
        "webview"
      ]
    }
  ];
  module.exports.metadata = {
    "cordova-plugin-whitelist": "1.3.4",
    "cordova-universal-links-plugin": "1.2.1",
    "kunder-cordova-plugin-webview": "2.8.0"
  };
});